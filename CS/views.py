__author__ = "Khashayar Ghamati"
__email__ = "khashayar@ghamati.com"


import json

import requests
import ast
from django.http import HttpResponseRedirect, HttpResponse
from django.views import generic
from django.contrib.messages.views import messages


class IndexView(generic.TemplateView):

    def post(self, request, *args, **kwargs):
        first_vector = ast.literal_eval(request.POST.get('first_vector'))
        second_vector = ast.literal_eval(request.POST.get('second_vector'))

        if first_vector and second_vector:
            result = self.send_request(
                first_vector=first_vector,
                second_vector=second_vector
            )
            print(result)
            messages.info(request, '{}'.format(result))

            return HttpResponse('Result is : {}'.format(result))
        return HttpResponseRedirect('/')

    def send_request(self, first_vector, second_vector):
        url = "http://127.0.0.1:8080/multiply/"
        param = json.dumps({"first_vector": first_vector, "second_vector": second_vector})
        headers = {'Content-type': 'application/json'}
        request = requests.post(
            url=url,
            data=param,
            headers=headers
        )

        return request.text
